import Link from 'next/link'
import Head from 'next/head'

import { API_URL } from '../../constants/api'
import styles from '../../styles/Post.module.css'

function Post({post}){
    return (
        <>
            <Head>
                <title>Blog d'éductation canine</title>
                <link rel="icon" href="/pixel.jpg" />
            </Head>
            <Link href="/">
                <a>
                    Retourner sur le menu
                </a>
            </Link>
            <div className={styles.container}>
                <div className={styles.post}>
                    <img src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/dog-puppy-on-garden-royalty-free-image-1586966191.jpg?crop=1.00xw:0.669xh;0,0.190xh&resize=1200:*"/>
                    <div className={styles.headerInformations}>
                        <span>8 Novembre 2021</span>
                        <span className={styles.category}>Catégories, Alimentation, Matériels, Soins</span>
                    </div>
                    <h1>{post.title}</h1>
                    <p>{post.body}</p>
                    <p>{post.body}{post.body}{post.body}{post.body}{post.body}{post.body}{post.body}{post.body}</p>
                </div>
            </div>
        </>
    )
}
export default Post

export async function getStaticPaths(){
    const posts = await fetch(API_URL).then(response => response.json())
    const paths = posts.map((post) => ({
      params: { id: post.id.toString() },
    }))
  
    return { paths, fallback: false }
}

export async function getStaticProps({ params }) {
    const post = await fetch(`${API_URL}/${params.id}`).then(response => response.json())
    return { props: { post } }
  }
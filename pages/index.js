import Head from 'next/head'
import Link from 'next/link'
import Image from 'next/image'

import { API_URL } from '../constants/api';

import styles from '../styles/Home.module.css'

export default function Home({posts}) {
  console.log('posts', posts);
  return (
    <div className={styles.container}>
      <Head>
        <title>Blog d'éductation canine</title>
        <link rel="icon" href="/pixel.jpg" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Mathilde CARREY
        </h1>
        <p className={styles.description}>
          Les conseils d'éducation canine
        </p>

        {posts.map(post => 
          <div className={styles.posts}>
          <Link href={`/blog/${post.id}`}>
              <a>
                <h3>{post.title}</h3>
                <p>{post.body}</p>
              </a>
            </Link>
          </div>
        )}
      
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}

export async function getServerSideProps(){
  const posts = await fetch(API_URL).then(response => response.json())

  return {
    props: {
      posts
    }
  }
}